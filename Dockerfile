FROM python:3.7

RUN mkdir /workspace
WORKDIR /workspace
ADD . /workspace/
RUN pip install -r requirements.txt

EXPOSE 5000
CMD ["python", "/workspace/app.py"]
